require 'yaml'
require 'stringex'
require 'time'
require 'middleman'
require 'gitlab'
require_relative './generators/direction'
require_relative './generators/releases'

# New tasks can be added direcly in this file
# or in a new file in lib/tasks with a .rake extension
# For example: lib/tasks/my_new_task.rake
Dir.glob('lib/tasks/*.rake').each { |r| load r }

begin
  require 'rspec/core/rake_task'

  RSpec::Core::RakeTask.new(:spec, :tag) do |t, task_args|
    t.rspec_opts = "--tag #{task_args[:tag] || '~@feature'}"
  end
rescue LoadError
  # no rspec available
end

task default: :spec

desc 'Begin a new post'
task :new_post, :title do |t, args|
  if args.title
    title = args.title
  else
    puts 'Enter a title for your post: '
    title = STDIN.gets.chomp
  end

  filename = "sites/uncategorized/source/blog/blog-posts/#{Time.now.strftime('%Y-%m-%d')}-#{title.to_url}.html.md.erb"
  puts "Creating new post: #{filename}"
  File.open(filename, 'w') do |post|
    post.puts '---'
    post.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    post.puts 'author: Firstname Lastname # if name includes special characters use double quotes "First Last"'
    post.puts 'author_gitlab: GitLab.com username # ex: johndoe'
    post.puts 'author_twitter: Twitter username or gitlab # ex: johndoe'
    post.puts 'categories: company'
    post.puts 'image_title: "/images/blogimages/post-cover-image.jpg"'
    post.puts 'description: "Short description for the blog post"'
    post.puts 'tags: tag1, tag2, tag3'
    post.puts 'cta_button_text: "Watch the <strong>XXX release webcast</strong> live!" # optional'
    post.puts 'cta_button_link: "https://page.gitlab.com/xxx.html" # optional'
    post.puts 'guest: false # required when the author is not a GitLab Team Member'
    post.puts 'ee_cta: false # required only if you do not want to display the EE-trial banner'
    post.puts 'install_cta: false # required only if you do not want to display the "Install GitLab" banner'
    post.puts "twitter_text: \"Text to tweet\" # optional;  If no text is provided it will use post's title."
    post.puts 'featured: yes # reviewer should set'
    post.puts '---'
  end
end

namespace :generators do
  desc 'Executes direction generator'
  task :direction do
    Generators::Direction.new.generate
  end

  desc 'Executes releases generator'
  task :releases do
    ReleaseList.new.generate($stdout)
  end
end

# Monthly release post
# https://about.gitlab.com/handbook/marketing/blog/release-posts/#monthly-releases
namespace :release do
  desc 'Creates the monthly release post'
  task :monthly do |t, args|
    puts 'Enter the GitLab version (major.minor format, example: 12.10): '
    version = STDIN.gets.chomp
    puts 'Enter the release post date (ISO format, example: 2020-05-22): '
    date = STDIN.gets.chomp

    abort('Aborted! You need to specify a minor version, like 12.1') unless /\A\d+\.\d+\z/.match?(version)
    abort('Aborted! You need to specify a valid release post date, like 2020-05-22') unless /\A\d{4}-\d{2}-22\z/.match?(date)

    # Various versions formats
    version_dash = version.tr('.', '-')
    version_underscore = version.tr('.', '_')
    branch_name = "release-#{version_dash}"

    # Abort if the release branch has already been created
    abort("Aborted! The branch #{branch_name} already exists") if `git branch | grep #{branch_name}`.tr("\n", '').strip == branch_name

    # Directories
    marketing_site_source_dir = File.expand_path('sites/uncategorized/source', __dir__)
    top_level_source_dir = File.expand_path('source', __dir__)
    source_releases_dir = "#{marketing_site_source_dir}/releases"
    data_releases_dir = File.expand_path('data/release_posts', __dir__)
    version_data_dir = "#{data_releases_dir}/#{version_underscore}"
    unreleased_data_dir = "#{data_releases_dir}/unreleased"

    # Templates
    mvp_template = "#{unreleased_data_dir}/samples/mvp.yml"
    cta_template = "#{unreleased_data_dir}/samples/cta.yml"

    # Stash modified and untracked files so we have a "clean" environment
    # without accidentally deleting data
    puts "Stashing changes"
    status = `git status --porcelain`
    `git stash -u` unless status.empty?

    # Sync with upstream master
    `git checkout master`
    `git pull origin master`

    # Create branch
    `git checkout -b #{branch_name}`

    #
    # Release post intro
    #
    intro_filename = "#{source_releases_dir}/posts/#{date}-gitlab-#{version_dash}-released.html.md"

    if File.exist?(intro_filename)
      abort('rake aborted!') if ask("#{intro_filename} already exists. Do you want to overwrite?", %w[y n]) == 'n'
    end

    puts
    puts "--------------------------------"
    puts "=> Creating new release post intro: #{intro_filename}"

    intro_text = File.read('doc/templates/blog/monthly_release_blog_template.html.md')
    intro_text.gsub!('X.Y', version)
    intro_text.gsub!('X-Y', version_underscore)

    File.open(intro_filename, 'w') do |post|
      post.puts intro_text
    end

    #
    # Front page announcement
    #
    frontpage_announcement_filename = "#{marketing_site_source_dir}/includes/home/ten-oh-announcement.html.haml"

    puts "=> Generating frontpage announcement: #{frontpage_announcement_filename}"

    frontpage_announcement_text = File.read('doc/templates/blog/monthly_announcement_frontpage.html.haml')
    frontpage_announcement_text.gsub!('X.Y', version)
    frontpage_announcement_text.gsub!('X-Y', version_dash)
    frontpage_announcement_text.gsub!('YYYY', date.split('-')[0])
    frontpage_announcement_text.gsub!('MM', date.split('-')[1])

    File.open(frontpage_announcement_filename, 'w') do |post|
      post.puts frontpage_announcement_text
    end

    #
    # Data directory
    #
    abort("Aborted! #{version_data_dir} already exists") if Dir.exist?(version_data_dir)
    puts "=> Creating new release post data directory: #{version_data_dir}"
    FileUtils.mkdir_p(version_data_dir)

    #
    # MVP file
    #
    puts "=> Creating #{version_data_dir}/mvp.yml"
    FileUtils.cp(mvp_template, "#{version_data_dir}/mvp.yml")

    #
    # CTA file
    #
    puts "=> Creating #{version_data_dir}/cta.yml"
    FileUtils.cp(cta_template, "#{version_data_dir}/cta.yml")

    # Add and commit
    `git add #{data_releases_dir} #{top_level_source_dir} #{marketing_site_source_dir}`
    `git commit -m 'Init release post for #{version}'`

    puts
    puts "=> You can now push the new branch with the following command:"
    puts
    puts "    git push origin #{branch_name}"
    puts
    puts "--------------------------------"
  end

  desc 'Creates the release post MR, supporting branches, and supporting MRs'
  task :start do |t, args|
    # Directories
    data_releases_dir = File.expand_path('data/release_posts', __dir__)
    unreleased_data_dir = "#{data_releases_dir}/unreleased"

    # Templates
    bugs_template = "#{unreleased_data_dir}/samples/bugs.yml"
    usability_improvements_template = "#{unreleased_data_dir}/samples/usability_improvements.yml"
    performance_improvements_template = "#{unreleased_data_dir}/samples/performance_improvements.yml"

    puts 'Enter the GitLab version (major.minor format, example: 12.10): '
    version = STDIN.gets.chomp
    puts 'Enter the release post date (ISO format, example: 2020-05-22): '
    date = STDIN.gets.chomp

    abort('Aborted! You need to specify a minor version, like 12.1') unless /\A\d+\.\d+\z/.match?(version)
    abort('Aborted! You need to specify a valid release post date, like 2020-05-22') unless /\A\d{4}-\d{2}-22\z/.match?(date)

    # Various versions formats
    version_dash = version.tr('.', '-')
    branch_name = "release-#{version_dash}"

    puts "Connecting to GitLab..."
    @gitlab = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: PRIVATE_TOKEN
    )
    @user = @gitlab.user
    puts "Connection successful. Connected user email: " << @user.email

    # Gather user inputs
    puts 'Enter @mention handle of the Release Post Manager (including the @ symbol): '
    release_post_manager = STDIN.gets.chomp
    puts 'Enter @mention handle of the TW Lead (including the @ symbol): '
    tw_lead = STDIN.gets.chomp
    puts 'Enter @mention handle of the Tech Advisor (including the @ symbol): '
    tech_advisor = STDIN.gets.chomp
    puts 'Enter @mention handle of the PMM Lead (including the @ symbol): '
    pmm_lead = STDIN.gets.chomp

    # Replace @mention instances
    template = File.open('.gitlab/merge_request_templates/Release-Post.md').read
    template = template.gsub("@release_post_manager", release_post_manager)
    template = template.gsub("@tw_lead", tw_lead)
    template = template.gsub("@tech_advisor", tech_advisor)
    template = template.gsub("@pmm_lead", pmm_lead)

    release_post_mr = @gitlab.create_merge_request(
      7764, "Draft: Release post - GitLab #{version}",
      {
        source_branch: branch_name,
        target_branch: 'master',
        description: template,
        remove_source_branch: true
      })

    # Sync with upstream master
    `git checkout master`
    `git pull origin master`

    #
    # Bugs branch and file
    #
    `git checkout -b #{branch_name}-bugs`
    puts "=> Creating #{unreleased_data_dir}/bugs.yml"
    FileUtils.cp(bugs_template, "#{unreleased_data_dir}/bugs.yml")
    `git add #{data_releases_dir}/bugs.yml`
    `git commit -m 'Add bugs.yml'`
    `git push --set-upstream origin #{branch_name}-bugs`

    #
    # Usability Improvements branch and file
    #
    `git checkout -b #{branch_name}-usability-improvements`
    puts "=> Creating #{unreleased_data_dir}/usability_improvements.yml"
    FileUtils.cp(usability_improvements_template, "#{unreleased_data_dir}/usability_improvements.yml")
    `git add #{data_releases_dir}/usability_improvements.yml`
    `git commit -m 'Add usability_improvements.yml'`
    `git push --set-upstream origin #{branch_name}-usability-improvements`

    #
    # Performance Improvements branch and file
    #
    `git checkout -b #{branch_name}-performance-improvements`
    puts "=> Creating #{unreleased_data_dir}/performance_improvements.yml"
    FileUtils.cp(performance_improvements_template, "#{unreleased_data_dir}/performance_improvements.yml")
    `git add #{data_releases_dir}/performance_improvements.yml`
    `git commit -m 'Add performance_improvements.yml'`
    `git push --set-upstream origin #{branch_name}-performance-improvements`

    bug_usability_performance_template = File.open('.gitlab/merge_request_templates/Release-Post-Bug-Performance-Usability-Improvement-Block.md').read
    bug_usability_performance_template = bug_usability_performance_template.gsub("@Release Post Manager", release_post_manager)
    bug_usability_performance_template = bug_usability_performance_template.gsub("@TW Lead", tw_lead)

    @gitlab.create_merge_request(
      7764,
      "Draft: release-#{version_dash}-bugs",
      {
        source_branch: "#{branch_name}-bugs",
        target_branch: 'master',
        description: bug_usability_performance_template,
        remove_source_branch: true
      })

    @gitlab.create_merge_request(
      7764,
      "Draft: release-#{version_dash}-usability-improvements",
      {
        source_branch: "#{branch_name}-usability-improvements",
        target_branch: 'master',
        description: bug_usability_performance_template,
        remove_source_branch: true
      })

    @gitlab.create_merge_request(
      7764,
      "Draft: release-#{version_dash}-performance-improvements",
      {
        source_branch: "#{branch_name}-performance-improvements",
        target_branch: 'master',
        description: bug_usability_performance_template,
        remove_source_branch: true
      })

    #
    # Create MVP Nominations Issue
    #

    mvp_issue_template = File.open('.gitlab/issue_templates/release-post-mvp-nominations.md').read
    @gitlab.create_issue(7764, "Release Post #{version} MVP Nominations", { description: mvp_issue_template, assignee_id: @user.id })

    #
    # Create Retrospective Issue
    #

    retro_issue_template = File.open('.gitlab/issue_templates/Release-Post-Retrospective.md').read
    retro_issue_template = retro_issue_template.gsub("ADD LINK", release_post_mr.web_url)
    @gitlab.create_issue(7764, "Release Post #{version} Retrospective", { description: retro_issue_template, assignee_id: @user.id })
  end

  # Do not use this task for major or minor releases that go out on 22nd
  desc 'Creates a new release post for patch versions'
  task :patch, :version do |t, args|
    version = args.version
    raise 'You need to specify a patch version, like 10.1.1' unless /\A\d+\.\d+\.\d+\z/.match?(version)

    destination = File.expand_path('sites/uncategorized/source/releases/posts', __dir__)

    date = Time.now.strftime('%Y-%m-%d')
    filename = "#{destination}/#{date}-gitlab-#{version.tr('.', '-')}-released.html.md"

    if File.exist?(filename)
      abort('rake aborted!') if ask("#{filename} already exists. Do you want to overwrite?", %w[y n]) == 'n'
    end

    puts "Creating new release post: #{filename}"

    template_text = File.read('doc/templates/blog/patch_release_blog_template.html.md.erb')
    template = ERB.new(template_text).result(binding)

    File.open(filename, 'w') do |post|
      post.puts template
    end
  end
end

desc 'Create a new press release'
task :new_press, :title do |t, args|
  data_dir = File.expand_path('data', __dir__)

  puts 'Enter a date for the press release (ISO format, example: 2016-12-30): '
  date = STDIN.gets.chomp
  puts 'Enter a title for the press release: '
  title = STDIN.gets.chomp

  filename = "source/press/releases/#{date}-#{title.to_url}.html.md"
  puts "Creating new press release: #{filename}"
  File.open(filename, 'w') do |pressrel|
    pressrel.puts '---'
    pressrel.puts 'layout: markdown_page'
    pressrel.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    pressrel.puts '---'
    pressrel.puts ''
  end

  press_yml = "#{data_dir}/press_releases.yml"
  puts 'Populating data/press_releases.yml'
  File.open(press_yml, 'a') do |yaml|
    yaml.puts ''
    yaml.puts "- title: \"#{title.gsub(/&/, '&amp;')}\""
    yaml.puts "  link: #{date}-#{title.to_url}.html"
    yaml.puts "  date: #{date}"
  end
end

desc 'Add an existing press release to the archive'
task :add_press, :title do |t, args|
  data_dir = File.expand_path('data', __dir__)

  puts 'Enter a date for the press release (ISO format, example: 2016-12-30): '
  date = STDIN.gets.chomp
  puts 'Enter a title for the press release: '
  title = STDIN.gets.chomp
  puts 'Enter the URL of the press release: '
  link = STDIN.gets.chomp

  press_yml = "#{data_dir}/press_releases.yml"
  puts 'Populating data/press_releases.yml'
  File.open(press_yml, 'a') do |yaml|
    yaml.puts ''
    yaml.puts "- title: \"#{title}\""
    yaml.puts "  link: #{link}"
    yaml.puts "  date: #{date}"
  end
end
