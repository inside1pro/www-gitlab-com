---
layout: job_family_page
title: UX Research Coordinator
---

At GitLab, UX Research Coordinators collaborate with our Product Designers, Product Managers, UX Researchers, and the rest of the community to manage participant recruitment for UX studies. UX Research Coordinators report to the UX Research Manager. Unless otherwise specified, all UX Research Coordinator roles at GitLab share the following responsibilities and requirements:

## Responsibilities

- Drive all aspects of the UX research coordination program across GitLab
- Maintain a strong working knowledge of the participant recruitment process
- Use GitLab for the intake of questions, concerns, and requests related to participant recruitment
- Maintain strong lines of communication with team members to relay status of requests
- Troubleshoot recruitment challenges as they arise
- Determine the best possible recruitment source for a given study
- Understand the pros and cons of each recruitment source
- Proactively report out key data
- Use participant panels for recruitment
- Stay aware of GDPR (General Data Protection Regulation) policies
- Develop and manage participant panel growth strategies
- Establish best practices for participant recruitment

## Requirements

* [Self-motivated and self-managing](/handbook/values/#efficiency), with strong organizational skills.
* Share our [values](/handbook/values/), and work in accordance with those values.
* Simultaneously manage multiple projects and time-driven tasks
* Strong communication and collaboration skills to keep teams informed on progress
* Empathetic, curious, and open-minded
* Ability to thrive in a fully remote organization
* Ability to use GitLab

### UX Research Coordinator (Intermediate)

The UX Research Coordinator (Intermediate) reports to the [UX Research Director](/job-families/engineering/ux-research-manager/#director-ux-research).

#### Job Grade

The UX Research Coordinator (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### UX Research Coordinator (Intermediate) Responsibilities

* Manage all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management.
* Grow, foster, and administer GitLab First Look, our participant recruitment database.
* Refine, document, and maintain processes related to participant recruitment.
* Collaborate with Product Designers, Product Managers, and UX Researchers to effectively understand their research goals and recruit appropriate participants.
* Be the Social Media Ambassador for UX Research at GitLab. Maintain social media accounts that promote GitLab's research efforts and aid participant recruitment.
* Manage relationships with third-party providers, such as recruitment platforms, digital rewards solutions, etc.
* Respond to inquiries from research participants.
* Create, maintain and communicate out monthly reports on key data points such as: budget, number of participants by research study type, and forecasting.
* Recruit participants from multiple sources.
* Administration of our research toolsets.

#### UX Research Coordinator (Intermediate) Requirements

* Some SQL query skills and willing to grow those skills.
* Exquisite organizational skills: regularly managing multiple research projects at a time.
* 2 years or more experience in UX Research Coordination or a related role that involves administration and coordination.
* In-depth experience with scheduling sessions and communicating with internal and external participants/stakeholders. 
* Excellent written and verbal communication skills.
* Experience with using a ticket-based system to track work requests.
* Able to use GitLab for communication and work management

### Senior UX Research Coordinator

The Senior UX Research Coordinator reports to the [UX Research Director](/job-families/engineering/ux-research-manager/#director-ux-research).

#### Senior UX Research Coordinator Job Grade

The UX Research Coordinator is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior UX Research Coordinator Responsibilities

* Extends that of the UX Research Coordinator (Intermediate) responsibilities
* Proactively identify and address recruitment challenges.
* Optimize how we recruit by learning more about our panel.  Manage what we know by creating and managing SQL databases.
* Create panel databases.
* Use databases to filter and screen  participants.
* Establish, drive, and/or identify social events to grow our participant panel.
* Use Social Media to proactively and routinely showcase UX Research insights and solutions completed by the team.
* Identify and implement ways to further improve efficiency and turn-around times in fulfilling requests.
* Formalize the research coordination effort into a program of work with extensive processes and documentation to improve visibility and increase efficiency.

#### Senior UX Research Coordinator Requirements

* Extends the UX Research Coordinator (Intermediate) requirements
* Strong SQL query skills.
* 5 years or more experience in UX Research Coordination or a related role that invovles administration and coordination, with at least 2 years experience in UX Research Coordination

## Performance Indicators

* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)
* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Selected candidates will be invited to schedule a 30-minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters. In this call, we will discuss your experience, understand what you are looking for in a UX Research Coordinator role, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
* Next, candidates will be scheduled for a 45-minute interview with a UX Researcher. 
* Finally, candidates will be invited to a 1-hour interview with our Director of UX Research.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).