---
layout: handbook-page-toc
title: Corporate Event Marketing at GitLab
description: 'For corporate event marketing at GitLab'
twitter_image:
twitter_image_alt: GitLab's Corporate Marketing Team Handbook
twitter_site: 'gitlab'
twitter_creator: 'gitlab'
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Mission Statement

- The mission of the Corporate Events Team is to:
    - Tell the GitLab story through engaging events
    - Make customers the hero
    - Build lasting and trusting vendor and internal relationships

## What does the Corporate Events team handle?

- **Sponsored events** 
    - Events with a global audience of 5000+ attendees for NA
    - 3000+ for other territories (50% or more of audience is national or global). 
    - A handful of smaller events that we handle due to the nature of the audience, product specific, and the awareness and thought leadership positions we are trying to build out as a company. 
    - The primary goal is always driving brand awareness but that cannot be the only result.
- **Owned events**
    - [GitLab Commit](/events/commit/), our User Conference
    - GitLab Culture Open House Events- by invite only
- **Internal events** 
    - [GitLab Contribute](/events/gitlab-contribute/), our internal company and core community event
    - Internal Sales events including [SKOs](/events/sko21/), Force Management planning, Rewards Travel, SQS, QBRs. Must be above 25 people attending for corp events involvement.
    Please review our events decision tree to ensure Corporate Marketing is the appropriate owner for an event. If it is not clear who should own an event based on the [decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing), please email events@gitlab.com.


## How We Evaluate and Build Potential Events

All GitLab events must check at least drive three or more of the aims of our events below to be considered.

- Brand awareness- we want to be a household name!
- Build community
- Evangelize a remote-first culture
- Gain contributors
- Share GitLab thought leadership
- Help with hiring great future teammates
- Gather new relevant leads/ drive ROI
- Educate possible buyers or users on our product or features
- Communicate the marketplace positioning
- Promote Partnerships and Alliances

### Corporate events must also meet:
{:.no_toc}

- Audience minimum requirement of 5000+ attendees NA, 3000+ for other territories (50% or more of audience is national or global)
- Audience demographic requirements. We consider the balance of roles represented (contributor, user, customer, potential hires), and the Global reach of the audience. Audience profile must be over 50% national/ global.

## Questions we ask ourselves when assessing an event:

- How and where will this event position us as a brand?
- Does this event drive business goals forward in the next quarter? Year?
- How does this event drive business goals forward in the new quarter? Year?
- Is the event important for the industry, thought leadership, or brand visibility? We give preference to events that influence trends and attract leaders/decision makers. We also prioritize events organized by our strategic partners.
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team-member or a member of the wider GitLab community.
- What type of people will be attending the event? We prefer events attended by diverse groups of decision makers with an interest in DevOps, DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We stress events that provide opportunities for meetings, workshops, booth and/or stands to help people find us, as well as create other interactions with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of support.
- What is the size of the opportunity for the event? We prioritize events based on their potential reach (audience size, the number of interactions we have with attendees) and potential for ROI (also account for cycle time).
- What story do we have to tell here and how does the event fit into our overall company strategy, goals, and product direction?
- Do we have the bandwidth and resources to make this event a success? Do we have the cycles, funds, collateral and runway to invest fully and make the event as successful as possible? Events must be weighed against other current activity in the region and department.

Suggested events will be subject to a valuation calculation - will it meet or exceed objectives listed above?

### For Corporate Marketing - Event Scorecard

Each question above is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 8 are not eligible for corporate sponsorship or financial support.
- Events scoring 10+ are given top priority for staffing, and resources.

| Criteria / Score | 0 | 1 | 2 |
| ---------------- | --- | --- | --- |
| Thought Leadership |  |  |  |
| Audience type |  |  |  |
| Attendee interaction |  |  |  |
| Location and Timing |  |  |  |
| Event Relevance/ Strategy |  |  |  |
| Brand Reach |  |  |  |
| Opportunity size/ Potential ROI |  |  |  |

We ask these questions and use this scorecard to ensure that we're prioritizing the GitLab's brand and our community's best interests when we sponsor events.

### Corporate Events Strategy / Goals

- **Brand**
    - For Sponsored Events: Get the GitLab brand in front of 15% of the event audience. 40,000 person event we would hope to get 4,000+ leads (10%) and 5% general awareness and visibility with additional branding and activities surrounding participation.
    - Human touches- Tracked by leads collected, social interactions, number of opportunities created, referrals, current customers met, and quality time spent on each interaction.
    - Audience Minimum Requirements- volume, relevance (our buyer persona, thought leaders, contributors), reach (thought leaders?), and duration of user/ buyer journey considered.
- **ROI**
    - Work closely with demand gen campaigns and field marketing to ensure events are driving results and touching the right audience.
    - Exceed minimum threshold of ROI for any events that also have a demand gen or field component- 5 to 1 pipe to spend within a 1-year horizon.
    - Aim to keep the cost per lead for a live event around $100.
    - [ROI Calculator](https://docs.google.com/spreadsheets/d/1SAYGXysUHGXPKrTDFf9yRcQrh9TYNxR9_Ts6H9dq8JY/edit?usp=sharing) we aim to make 5x ROI on pipeline focused events but this can be used to estimate what return we might get on an event.
- **Thought Leadership and Education**


## Meet the Corporate Events team

[**Karen Hartline**](https://about.gitlab.com/company/team/#khartline)

- Title: Director, Corporate Events 
- GitLab handle: @khartline
- Slack handle: @Karen Hartline

[**Lauren Conway**](https://about.gitlab.com/company/team/#Lconway)

- Title: Senior Corporate Events Manager
- GitLab handle: @lconway
- Slack handle: @Lauren Conway

[**Emily Chin**](https://about.gitlab.com/company/team/#echin)

- Title: Senior Corporate Events Production Manager
- GitLab handle: @echin
- Slack handle: @emilybchin

[**Erica Parker**](https://about.gitlab.com/company/team/?department=brand-activation#eparker9086)

- Title: Senior Corporate Events Manager
- GitLab handle: @eparker9086
- Slack handle: @EParker

[**Amanda Shen**](https://about.gitlab.com/company/team/?department=corporate-marketing#amandawshen)

- Title: Corporate Events Coordinator
- GitLab handle: @amandawshen
- Slack handle: @Amanda Shen


## GitLab Commit User Conferences

- Commit Virtual 2021- [Planning Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1595)
- Goal Call for proposals (CFP) and agenda-setting process:
    - 9+ months pre-event: Open an internal call for track issue (GitLab team members)
    - 7+ months pre-event: Select Commit track(s)
    - 6+ months pre-event: Open a public call for proposals (CFP) form and invite contributions
        - Track manager trainings and establishment of deadlines and workflows
    - 5+ months pre-event: Convene the CFP selection committee
    - 4+ months pre-event: Early session acceptances begin
    - 3+ months pre-event: CFP closes; Next round of acceptances sent
    - 2+ months pre-event: Final acceptances sent; sponsorships close
        - Any submissions not selected are thanked and – should the proposal be a potential fit for a future event – invited to stay in touch for participation at a later date
        - Draft agenda is reviewed by stakeholders for strength and diversity of content and speakers
    - 1+ month pre-event: Agenda is final and publishable
- GitLab's event experience is unique for speakers in the hands-on approach that the Corporate Events team takes to ensure that speakers feel supported and prepared. A typical [Commit speaker experience](https://about.gitlab.com/events/commit/speakers/) will include:
    - Access to a speaker page with all relevant deadlines, resources, and contact information
    - One-on-one access to a presentation support team, including the session track manager and production manager
    - Dedicated working document or email thread between the speaker, track manager, and production manager to work through final session title, description, and slide content
    - 2-3 Zoom meetings to discuss session flow and practice for timing, pace, and polish
    - Weekly communication and updates
    - Optional training and slide creation support
- Commit Virtual for GitLab team members:
    - We need support the day of the event with such tasks as chat moderator, booth staffing, social monitoring support...
- Onsite at Commit for GitLab team members:
    - You will be assigned one or multiple onsite tasks. It is critical you show up for your set duty and communicate any changes in your plans. Clean your schedule on the day of the event, as it will be a full day commitment.
        - Tasks include:
            - Track Scanning- it is essential you show up and stay for this if you are assigned. Our partners have paid to get leads from their talks and it is our promise to provide said leads. All talk attendees must be scanned for this purpose.
            - Check in Support
            - Swag table
            - Questions/ help desk
            - Booth Duty (Hiring, UX, Support, Security, Demo..) - do not leave the booth unstaffed. We have back up. Ask for help on coverage if you need it.
    - Dress code: casual to business casual. Wear what you feel comfortable in. No open toed shoes for safety reasons.
    - Team Travel
        - Team members may come in the day before the event and stay the night of the event. No additional days will be covered unless you have arranged a special circumstance with the Commit planning team.
        - We can only provide Visa support for speakers and external attendees for this event series.
        - If you live within 60 miles of the event you will be asked to commute to the event unless you have a specific arrangement with the Commit planning team.

### RFP Process

- See finance handbook for when you need to go through RFP process
- Use RFP Templates for uniform evaluation of vendors
    - [Questionnaire](https://docs.google.com/spreadsheets/d/154wEnFfBBOq0l_dq23LfGBWI1JHPwL1U_ERDZNjF67k/edit?usp=sharing) to send to vendors bidding
    - [DMC RFP Template](https://docs.google.com/document/d/1dGEffBse3FMnsM3YSVDUs848AU7Rn4DJRfn2l_nTcTQ/edit?usp=sharing)
    - Questions on using the templates ask them in the corporate-events-team Slack channel.

### Event Execution

For event execution instructions, please see the [Marketing Events page](/handbook/marketing/events/#event-execution) for detailed instruction and the criteria used to determine what type of events are supported.

For both GitLab-owned and external events, speakers and content DRIs should build in steps for legal approvals from necessary parties on public-facing presentations before materials are due.

### Best Practices on site at a GitLab event

- [Employee Booth Guidelines](/handbook/marketing/events/#employee-booth-guidelines)
- [Scanning Best Practices](/handbook/marketing/events/#scanning-best-practices)

## Accessibility at GitLab Hosted Events

### In advance of the event, we promise that we will take into account the following:

- Accessibility will not be an afterthought.
- Events will be inclusive and accessible.
- Venues will meet International Accessibility Standards guidelines.
- There will be seating available and accessible seating made available upon request
- There will be gender neutral pronouns in event communication.
- General dress guidance will not include male/female binary descriptors, and attendees can decide for themselves what works best for them.
- Feedback will be collected during and after the event to gauge accessibility and comfort levels.
- Designated confidential resources will be available for team members.
- Name badges will have write-in areas, printed or stickers for preferred pronouns.
- Large group meals will have ingredients and allergens listed. If you have communicated your needs with the event staff and the kitchen cannot provide a suitable meal for you, there will be a designated amount you can expense per meal on site.
- The Events team will commit to the idea that no detail is too small
- For all GitLab hosted events of 300+ we hire security that is also certified in first aid.

### Communicating Accessibility

- Gathering attendee needs during registration.
    - Dietary restrictions: If you have any questions, please follow up with the individual privately. Determine the specific restrictions and provide information on how ingredients will be provided. Offer solutions in case there is an issue onsite so that the attendee is prepared.
    - Additional needs: Include an open text area in which attendees can list specific needs to help ensure full guest participation. Example requests include:
        - Interpreting services
        - Assistive listening devices
        - Accessible parking 
        - Accessible hotel rooms
        - Captioning
        - Reserved front row seat
        - Wheelchair access to working tables throughout the room
        - Lactation room
        - Seating for in-person events
        
The team will make all good faith efforts to carry out requests, or will open lines of discussion for options when not available.

    - Preferred pronouns: Aim to avoid a closed field for gender and instead provide a blank write-in field or inclusive dropdown options so attendees can select/type their preference pending on the badging system. This information will be displayed on name badges, or available to add with stickers etc.
    Email and landing page communication
- Put accessibility information in the event page footer or have a page dedicated to accessibility.
- Send out email on accessibility specifics and resources in advance of the event. Examples of what to communicate in that email:
    - Use of flash photography
    - Any sort of strobe lights or flashing images that may cause seizures
    - Distinctly amplified sounds/music
    - The use of fog machines/any other chemicals or smells that may make your space inaccessible to individuals with Multiple Chemical Sensitivity (MCS) or Idiopathic Environmental Intolerances (IEI)
    - Whether interpreting services will be provided for various speakers, panels, talks, etc.
    - Whether assistive listening devices will be provided.
    - All optional parts of your event, including off-site social activities, that may not be fully accessible.
    - Information about meals and dietary restrictions.
    - Accessible transportation options
    - First Aid/Medical Assistance options
    - Info on how to get in touch with questions or concerns.

### For speakers

- Speak clearly (ideally facing forward without covering your mouth)
- Avoid acronyms and colloquialisms as much as possible
- When addressing someone specifically, ask for his/her/their name and confirm pronouns
- Specify when you’re finished speaking
- For interpreters, always look at and address the participating attendee
- Repeat questions posted by the audience before responding, especially if there is not a roving microphone available.

## Virtual Events at GitLab

At GitLab, the Corporate Events Marketing team owns events that meet specific criteria (link), including those events that have a global hybrid or virtual element. By design, virtual events at GitLab are not a simple video call nor are they a static playlist. GitLab virtual events aim to retain the same [brand personality](​​/handbook/marketing/corporate-marketing/#brand-personality), engagement, inclusivity, and fun as our in-person events.

### Lead time

A GitLab virtual event requires **16+ weeks of lead time** in order to accommodate the successful planning, scripting, set-up, recording, editing, and executing of a program.

### What does a virtual GitLab event entail?

Virtual events require considerable lead time and effort to plan, especially if there are any planned live elements.

- **Branding:** Requesting event-specific branding, including event logo, slide deck template, brand kit, and brand colors
- **Platform:** Determining a virtual event platform to allow for maximum engagement, clarify, and streaming stability
- **Content:** Collaborating with event content owners to develop messaging, content arc, and other thematic elements
- **Community:** Working with event content owners to invite community speakers and source subject matter experts
- **Communications:** Building content for event landing pages, registration, speaker communications, cross-functional promotion, and internal team training
- **Budget:** Fleshing out budget and sponsorship
- **Support:** GitLab speaker support and content development, as outlined in the [Commit Speaker page](/events/commit/speakers/)
- **Production:** Selecting production partnersto provide support on speaker recording, editing, live-streaming, and entertainment
- **Approval** : Ensuring all speakers and content undergo rigorous review and necessary approvals, as dictated by both GitLab Legal and speaker or partner requirements
- **Follow-up:** Collaborating with Social Media, Digital Production, Marketing Ops, Content, Sales Development, Customer Reference, Community, Product Marketing, and Legal to allow for adequate post-event planning

#### Why Does GitLab Pre-Record Content?

##### Benefits of pre-recording

- Allows for multiple takes and editing/ splicing any errors
- Allows for a polished delivery, increased professionalism, and addition of production elements
- Encourages real-time Q&amp;A via session chat so attendees can ask questions as they come up rather than saving all questions for the end
- Enables speakers to enjoy the day-of content rather than focusing solely on their presentation(s)
- Reduces the risk of wifi/ server/ bandwidth-related issues, providing stability and security
- Reduces stress for speakers; less focus on timing, connectivity, Q&amp;A, etc.

##### How pre-recording for a virtual event **increases** engagement

- Speakers can call-out or otherwise acknowledge individuals in the session chat, where seeing faces might be challenging in-person
- Speakers can answer questions as they come in, rather than waiting until the end when attendees may have forgotten or left
- Link to any resources as needed
- If there are no questions, feel free to post an FAQ (and answer) or 2
- Gamefy it! Any cool trivia or info that you didn&#39;t have time to cover in your talk? Add nuggets in the chat throughout the session
  - &quot;I&#39;m going to sprinkle in the chat 4 little-known facts about Kubernetes -- get ready!&quot;

#### Execution

In order to create a premium virtual event experience, the Corporate Events team executes virtual events in order to maximize global audience participation and engagement. The considerations in place include:

- Either timezone specific programming, or multiple segments to cover global timezones
- Events team online before, throughout, and after the event to ensure coverage and support
- A minimum of 2+ weeks for editing and captioning of content: Because GitLab&#39;s content is technical and its community is so passionate, Events is committed to ensuring precise captioning so that all attendees can contribute
- Live-stream stability and fall-back planning: As a fully virtual and remote-first organization, we believe in the power of asynchronous work

#### Accessibility

The Corporate Event Marketing team believes that everyone can contribute to virtual events, and so prepares for content to be varied, accessible, and time appropriately to enable as much mental and physical engagement as possible. Plans include:

- Generating human-reviewed captions for all sessions
- Building in breaks that are fun and energizing
- Making session recordings available post-event for on-demand consumption
- Ensuring a variety of sessions and content to encourage contribution from all organizations and business functions

### Best practices

Having run several global-focused virtual events, the GitLab team has some [best practices and recommendations to share](https://docs.google.com/document/d/1rPvewsTWm8uqGv-6Wr4-_j4ZmBVjL75fU5_YGV98d24/edit?usp=sharing) with prospective speakers.

## Coming soon

- **Supplier/ Vendor diversity list that encourages the use of LGBTQ-, woman-, veteran-, or minority-owned businesses.**
- **Venue Sourcing Accessibility Checklist**

