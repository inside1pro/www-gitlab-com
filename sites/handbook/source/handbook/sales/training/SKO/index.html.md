---
layout: handbook-page-toc
title: "Sales Kickoff"
description: "The GitLab Sales Kickoff sets the tone for the GitLab field organization for the new fiscal year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Kickoff (SKO) Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# Sales Kickoff 2022
GitLab SKO 2022 will take place from February 7-11, 2022 in National Harbor, MD USA at the [Gaylord National Resort & Convention Center](https://www.marriott.com/en-us/hotels/wasgn-gaylord-national-resort-and-convention-center/overview/) (just under 10 miles south of Washington, DC).

## Do you need a Visa Invitation Letter for SKO 2022?

If you are **employed via a GitLab entity**, please follow the process below.
   1. Fill out this [form](https://forms.gle/8r934fWiYATmCP8k9)
   1. Within minutes, you should receive an email to your inbox with a PDF visa letter attached.

If you are a **contractor**, please follow the process below.
   1. Fill out this [form](https://forms.gle/uNViiMt1A6h6Q6SK8)
   1. Within minutes, you should receive an email to your inbox with a PDF visa letter attached.

If you are **employed via a PEO**, you will need to reach out to your PEO to receive this letter. All PEOs have been given a template for them to use for this purpose.

If you have any further questions regarding a visa invitation letter, please reach out in `#people-connect`.

This page will be updated as additional details are confirmed.

----

## [Sales Kickoff 2020](/handbook/sales/training/SKO/2020)

## [Sales Kickoff 2021](/handbook/sales/training/SKO/2021)

----

## Sales Kickoff Planning 

For more information about the Sales Kickoff planning core team and process, see the [Sales Kickoff Planning](https://about.gitlab.com/handbook/sales/training/SKO/SKO-planning/) page.
