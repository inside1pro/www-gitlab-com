---
layout: handbook-page-toc
title: Slack Training
description: "Details on how to sign up for an in-app Slack training for new team members at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sign-up

Please review the learning objectives, requirements, and training details before signing up.

Sign up for the Slack training by completing [this Google form](https://docs.google.com/forms/d/e/1FAIpQLSe1pBLLFYOuoAyKssl9BtVGNlQXP08YYcudrlsKu0lic6apWQ/viewform?usp=sf_link). After completing the form, the Learning and Development team will reach out with next steps.

## Learning Objectives

GitLab team members participating in this training will:

1. Build confidence in their ability to use Slack as a form of informal and formal communication at GitLab.
1. Improve efficiency using tools built into the Slack app.
1. Contribute to reaching our target of [percent of messages that are not DMs](/handbook/communication/#why-we-track--of-messages-that-are-not-dms).
1. Review and reinforce GitLab values within the scope of Slack.

## Requirements

1. You're a GitLab team member (this is not available to external learners)
1. You can commit ~5 minutes per day for 10 business days to review and take action on tasks in Slack
1. You commit to completeing a post-training Google form to provide feedback on your experience

No prior experience with Slack or GitLab is needed to participate. The content reviewed is directed to brand new users in the GitLab Slack instance, however, team members are welcome to participate regardless of time at the company.

If you meet these requirements, please use [this Google form](https://docs.google.com/forms/d/e/1FAIpQLSe1pBLLFYOuoAyKssl9BtVGNlQXP08YYcudrlsKu0lic6apWQ/viewform?usp=sf_link) to sign-up.

## Training Details

Here's what to expect in the Slack training:

1. After signing up, you'll be added to a training cohort. The current goal is to run at least one cohort per month.
1. 3 days before the training begins, you'll be added to a group direct message with a notification of the training start date and first instructions
1. The training will run for 10 business days
1. Each day, you'll get one direct message with your daily task instructions
1. Messages will be scheduled by the L&D team and are triggered to send at 2pm UTC

## Metrics

We measure success using the following metrics:

| Metrics | How we measure | Goal |
| ----- | ----- | ----- |
| Percentage of new team members who complete the training per quarter | Completions of end of program survey | 30% |
|  Impact on target of [percent of messages that are not DMs](/handbook/communication/#why-we-track--of-messages-that-are-not-dms) | Tracked on [Slack handbook page](/handbook/communication/#why-we-track--of-messages-that-are-not-dms) | Correlation of course completions and target |

## Future improvements

The following topics will be prioritized in future iterations of this training:

1. Increase automation - explore tools and plugins that can automatically assign and trigger this training based on team member start date
1. Improve metrics - track impact of training on team member behavior in Slack

## Questions

Reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) with any questions.

