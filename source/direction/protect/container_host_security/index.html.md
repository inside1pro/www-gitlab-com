---
layout: secure_and_protect_direction
title: "Category Direction - Container Host Security"
description: "Container Host Security (CHS) refers to the ability to detect, report, and respond to attacks on containerized infrastructure and workloads."
canonical_path: "/direction/protect/container_host_security/"
---

- TOC
{:toc}

## Protect

| | |
| --- | --- |
| Stage | [Protect](/direction/protect/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2021-10-20` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Container Host Security in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute. We welcome feedback, bug reports, feature requests, and community contributions.

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContainer%20Behavior%20Analytics) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/3160) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for container security, we'd especially love to hear from you.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal%20-%20detailed) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::protect" ~"Category:Container Host Security" ~"group::container security"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

We believe [everyone can contribute](https://about.gitlab.com/company/mission/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Container Host Security (CHS) refers to the ability to detect, report, and respond to attacks on containerized infrastructure and workloads. Techniques include use of one or more types of intrusion detection systems (IDS) to detect attacks.  The IDS may be supplemented with custom-built monitoring capabilities and/or behavior analytics to improve the efficacy and scope of detected attacks.

An IDS is a device or software application that monitors a network or systems for malicious activity or policy violations. Malicious activity can then be reported back to an Administrator either through GitLab or through a security information and event management (SIEM) system. IDS types range in scope from single computers to large networks. The most common classifications are network intrusion detection systems (NIDS) and host-based intrusion detection systems (HIDS). Some leverage honeypots to attract and characterize malicious traffic. Some strictly leverage signature-based detection, while others use machine learning to automatically detect anomalies.

An ideal Container Host Security solution would include all types of intrusion detection systems to provide defense-in-depth and protection against a wide range of attacks.  Additional analytics can be layered on top of the data collected from an IDS to help filter out false positives and to recommend new rules to reduce false negatives.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst) and [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) are our primary target personas for any organizations that have an established security team
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) could be a secondary persona for organizations without established security teams.  This needs to be researched more.

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
We are planning to build a Container Host Security solution that is cloud native, easy to use, and tightly integrated with the rest of GitLab.  Our underlying architecture will combine several technologies to create a full-featured solution while also simplifying and unifying the management experience to look and feel like a single, easy-to-use product.  We plan to be both a host-based IDS and an IPS, allowing users to choose to either log, alert, or block any activity that is detected in their containerized environments.

Some of the top detection and protection capabilities that we provide today include application allow listing and file integrity monitoring.  To better manage these capabilities, we plan to provide an intuitive policy editor to simplify the administration of the tool.  We also plan to surface actionable alerts and logs inside GitLab to allow for a simple triage and response workflow to detected attacks.  Longer-term we plan to add additional behavior analytics on top of our host security to improve our threat detection capabilities.

#### Key goals and guiding principles

1. **Engage security teams** - build a pathway to get security departments involved in GitLab
1. **Add value to open source** - Limit who can make policy changes; support auditing and approvals
1. **Unified experience** - Provide a consistent user experience that abstracts away the choice of underlying technologies
1. **Single app vision** - Tightly integrate with the rest of GitLab to drive cross-stage adoption and to allow scan results to inform enforcement decisions
1. **Nail it then scale it** - Get the solution right for containerized Kubernetes applications first - then extend that support to other platforms (serverless, OpenShift, bare-metal, VMs)

#### What is our Vision (Long-term Roadmap)

We are temporarily deferring work on this category in favor of accelerating our Security Orchestration and Container Scanning categories.  We plan to resume work mid-2021.

#### What's Next & Why (Near-term Roadmap)
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
The next step for Container Host Security is to allow these capabilities (Falco, AppArmor, and Pod Security Policies) to be installed through the GitLab Agent for Kubernetes.  When we resume work in this area, we plan to prioritize an integration with the GitLab Agent for Kubernetes first.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->
We are not currently planning to do the following:
*  Build our own SIEM

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
* [Planned to Viable](https://gitlab.com/groups/gitlab-org/-/epics/3163)

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
We plan to measure the success of this category based on the total number of monthly alerts generated by our Container Host Security solution across our entire customer base.

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->
Top competitors:
* [Prisma Cloud (formerly Twistlock)](https://www.twistlock.com/)
* [Sysdig](https://sysdig.com/)
* [Aqua Secure](https://www.aquasec.com/products/aqua-cloud-native-security-platform/)
* [Trend Micro](https://www.trendmicro.com/en_us/business/products/hybrid-cloud.html#t3)

Key features offered by competitors:
* Application Allow Listing
* Active Response
* Compliance
* Exploit Protection / RASP
* File Integrity Monitoring
* IAM
* Kubernetes Support
* Log Monitoring
* Malware Scanning
* Serverless Support
* Vulnerability Scanning

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->
Gartner defines two markets that are relevant to this category:
1. [Intrusion Detection and Prevention Systems](https://www.gartner.com/en/documents/3844163/magic-quadrant-for-intrusion-detection-and-prevention-sy0) market
1. [Cloud Workload Protection Platforms (CWPP)](https://www.gartner.com/en/documents/3906670/market-guide-for-cloud-workload-protection-platforms) market

Of these two markets, the second aligns more closely with where we are headed as we are focused on cloud and containerized workload protection rather than attempting to be a generic IDS/IPS for all types of workloads.
<!--
### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.--
TODO

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.--
TODO

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.--
TODO-->

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
We will need to integrate an IDS as an important first step toward our strategy.

Additional strategy items will be uncovered as we do more research in this area.

### Related Categories
<!-- What's the most important thing to move your strategy forward?-->
*  [Kubernetes Management](/direction/configure/kubernetes_management/)
